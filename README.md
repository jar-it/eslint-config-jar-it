# eslint-config-jar-it

Our base eslint configuration.

## Installation

Install this package, and save it as a devDependency:

```
npm install --save-dev eslint-config-jar-it
```

Then have your project's `.eslintrc` file extend the ruleset.

```json
{
    "extends": "jar-it"
}
```

If you're using Vue in your project, you'll need to extend the Vue extension of the configuration, and install the eslint Vue and html plugins:

```json
{
    "extends": "jar-it/vue"
}
```

```
yarn add eslint-plugin-vue eslint-plugin-html --dev
```

## Common Settings Cookbook

### Globals

```json
{
    "globals": {
        "$": true
    }
}
```

### Environments

```json
{
    "env": {
      "browser": true,
      "node": true
    }
}
```

## About JAR IT

JAR IT is a web consultancy based in Phoenix, AZ.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
